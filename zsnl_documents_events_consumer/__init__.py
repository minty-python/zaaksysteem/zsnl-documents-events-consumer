# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

__version__ = "0.0.1"

__all__ = ["DocumentsEventsConsumer"]
